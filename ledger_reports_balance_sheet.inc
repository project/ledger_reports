<?php

/**
 * Create a table, summarizing the current assets, equity, and liabilities.
 */
function ledger_reports_balance_sheet($book = 1) {
  if ($book == "") {
    $book = 1;
  }
  // Sanitize user input.
  $book = intval($book);

  // Set the page title.
  drupal_set_title(t('Balance Sheet'));

  // Create the breadcrumb for the page.
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Ledger'), 'ledger'),
    l(t('Reports'), 'ledger/reports'),
  );
  drupal_set_breadcrumb($breadcrumb);

  // Only pull transactions from the following accounts.
  $accounts = array('assets', 'equity', 'liabilities');
  $sql = ledger_reports_create_sql($accounts);
  $results = db_query($sql, array(':book' => $book));

  // Initialize variables.
  $rows = array();
  $sum = 0;
  $assets = 0;
  $liabilities = 0;
  $equity = 0;
  $first_liability = TRUE;
  $first_equity = TRUE;
  $printed_asset_total = FALSE;
  $printed_liability_total = FALSE;

  // Iterate through all accounts. Print each account total and the totals.
  // of each account type.
  foreach ($results as $v) {
    if ($v->balance <> 0) {
      $sum += $v->balance;
      if ($v->type == 'assets') {
        $assets += $v->balance;
      }
      elseif ($v->type == 'liabilities') {
        $liabilities += $v->balance;
        if ($first_liability) {
          $first_liability = FALSE;
          $rows[] = ledger_reports_print_account_total(t('Total Assets'), $assets);
          $printed_asset_total = TRUE;
        }
      }
      else {
        if ($first_equity) {
          $first_equity = FALSE;
          if (!$printed_asset_total) {
            $rows[] = ledger_reports_print_account_total(t('Total Assets'), $assets);
            $printed_asset_total = TRUE;
          }
          $rows[] = ledger_reports_print_account_total(t('Total Liabilies'), $liabilities);
          $printed_liability_total = TRUE;
        }
        $equity += $v->balance;
      }
      $rows[] = array(
        array(
          'data' => drupal_ucfirst($v->type),
          'align' => 'left',
          'class' => 'ledger_reports_cell',
        ),
        array(
          'data' => check_plain($v->name),
          'align' => 'left',
          'class' => 'ledger_reports_cell',
        ),
        array(
          'data' => $v->balance,
          'align' => 'right',
          'class' => 'ledger_reports_cell',
        ),
      );
    }
  }
  if (!$printed_asset_total) {
    $rows[] = ledger_reports_print_account_total(t('Total Assets'), $assets);
  }
  if (!$printed_liability_total) {
    $rows[] = ledger_reports_print_account_total(t('Total Liabilities'), $liabilities);
  }
  $rows[] = ledger_reports_print_account_total(t('Total'), $sum);

  $header = array(t("Type"), t("Name"), t("Balance"));
  $module_path = drupal_get_path('module', 'ledger_reports');
  $output = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attached' => array(
      'css' => array($module_path . '/' . 'ledger_reports.css' => array()),
    ),
  );
  return $output;
}
