<?php

/**
 * Create a table listing the changes in income and expense during a year.
 */
function ledger_reports_income_statement($book = 1, $year = NULL) {
  // Unless specified otherwise, view the current years report.
  if (!isset($year) || $year == "") {
    $year = date("Y");
  }
  if ($book == "") {
    $book = 1;
  }

  // Sanitize user input.
  $book = intval($book);
  $year = intval($year);

  // Set the page title.
  drupal_set_title(t('Income Statement for @year', array('@year' => $year)));

  // Create the breadcrumb for the page.
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Ledger'), 'ledger'),
    l(t('Reports'), 'ledger/reports'),
  );
  drupal_set_breadcrumb($breadcrumb);

  // Initialize $output array and save the form to the render array.
  $output = array();
  $vars = array('book' => $book, 'callback' => 'income-statement');
  $output['form'] = drupal_get_form('ledger_reports_year_select_form', $vars);

  // Create the report table.
  $accounts = array('income', 'expenses');
  $sql = ledger_reports_create_sql($accounts, $year);
  $results = db_query($sql, array(':book' => $book, ':year' => $year));
  $rows = array();
  $revenue = 0;
  $expenses = 0;
  $sum = 0;
  $first_expense = TRUE;
  $printed_revenues = FALSE;

  // Iterate through the results, sum the balances to make the total, and save.
  // each row in the $rows array.
  foreach ($results as $v) {
    if ($v->type == "income") {
      $revenue -= $v->balance;
    }
    else {
      if ($first_expense) {
        $first_expense = FALSE;
        $rows[] = ledger_reports_print_account_total(t("Total Revenue"), $revenue);
        $printed_revenues = TRUE;
      }
      $expenses -= $v->balance;
    }
    $sum -= $v->balance;
    $rows[] = array(
      array(
        'data' => drupal_ucfirst($v->type),
        'align' => 'left',
        'class' => 'ledger_reports_cell',
      ),
      array(
        'data' => $v->name,
        'align' => 'left',
        'class' => 'ledger_reports_cell',
      ),
      array(
        'data' => -1 * $v->balance,
        'align' => 'right',
        'class' => 'ledger_reports_cell',
      ),
    );
  }

  // If the revenue line has not been printed (ie, no expenses) print it.
  if (!$printed_revenues) {
    $rows[] = ledger_reports_print_account_total(t("Total Revenue"), $revenue);
  }

  // Add the expenses total row on the bottom.
  $rows[] = ledger_reports_print_account_total(t("Total Expenses"), $expenses);

  // Add the "total" row on the bottom.
  $rows[] = ledger_reports_print_account_total(t("Net Income"), $sum);

  // Create the table header.
  $header = array(t("Type"), t("Name"), t("Balance"));

  // Get the module patch so we can include the custom CSS
  $module_path = drupal_get_path('module', 'ledger_reports');

  // Save all the pieces to the table array.
  $table = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attached' => array(
      'css' => array($module_path . '/' . 'ledger_reports.css' => array()),
    ),
  );

  // Save the table to the render array.
  $output['table'] = $table;
  return $output;
}
