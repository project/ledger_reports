<?php

/**
 * Display a pie chart, breaking down the money in an account category.
 */
function ledger_reports_account_pie($account, $book = 1, $year = NULL) {

  // Unless specified otherwise, view the current years report.
  if (!isset($year) || $year == "") {
    $year = date("Y");
  }
  if ($book == "") {
    $book = 1;
  }

  // Sanitize user input.
  $book = intval($book);
  $year = intval($year);

  // Set the page title.
  $string_replacement = array('@account' => ucfirst($account), '@year' => $year);
  drupal_set_title(t('@account Pie Chart for @year', $string_replacement));

  // Create the breadcrumb for the page.
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Ledger'), 'ledger'),
    l(t('Reports'), 'ledger/reports'),
  );
  drupal_set_breadcrumb($breadcrumb);

  // Initialize $output array and save the form to the render array.
  $output = array();
  $vars = array('book' => $book, 'callback' => "{$account}-pie");
  $output['form'] = drupal_get_form('ledger_reports_year_select_form', $vars);

  // Create the report table.
  $accounts = array();
  $reverse = TRUE;
  switch ($account) {
    case 'income':
      $accounts[] = 'income';
      break;

    case 'expense':
      $accounts[] = 'expenses';
      $reverse = FALSE;
      break;

    case 'asset':
      $accounts[] = 'assets';
      $year = NULL;
      $reverse = FALSE;
      break;
  }
  $sql = ledger_reports_create_sql($accounts, $year);
  $results = db_query($sql, array(':book' => $book, ':year' => $year));
  $variables = ledger_reports_pie($results, 'balance', $reverse);
  $flot_graph = array(
    '#theme' => 'flot_graph',
    '#data' => $variables['data'],
    '#options' => $variables['options'],
    '#legend' => TRUE,
    '#element' => $variables['element'],
  );
  $output['flot_graph'] = $flot_graph;
  return $output;
}

/**
 * Create the flot object from the account data.
 */
function ledger_reports_pie($results, $column, $reverse = TRUE) {
  $data = array();
  foreach ($results as $row) {
    $balance = ($reverse ? -1 : 1) * $row->balance;
    if ($balance > 0) {
      $data_point  = new flotData($balance);
      $data_point->label = check_plain($row->name);
      $data_point->pie = new flotPie();
      $data[] = $data_point;
    }
  }
  $variables = array(
    'data' => $data,
    'element' => array(
      'id' => 'flot-example-normal',
      'class' => 'flot-example',
      'style' => "width:600px;height:400px",
    ),
    // Display the data as a pie.
    'options' => array(
      'pie' => TRUE,
    ),
  );

  return $variables;
}
