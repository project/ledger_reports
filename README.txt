CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Requirements

INTRODUCTION
------------

Current Maintainer: Kevin Nowaczyk <beakerboy@yahoo.com>

This module provides standard accounting reports from the data in the Drupal
Ledger module.

INSTALLATION
------------

Installation should be done via the online Drupal module installer.
The FLOT module is required above version 7.x-1.x-dev
This module also requires the Ledger Module.
