<?php

/**
 * Create a list of links to all the reports.
 */
function ledger_reports_page() {

  // Create the breadcrumb for the page.
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Ledger'), 'ledger'),
  );
  drupal_set_breadcrumb($breadcrumb);

  // Create links to each report.
  $items = array(
    l(t('Balance Sheet'), 'ledger/reports/balance-sheet'),
    l(t('Income Statement'), 'ledger/reports/income-statement'),
    l(t('Income Pie Chart'), 'ledger/reports/income-pie'),
    l(t('Expense Pie Chart'), 'ledger/reports/expense-pie'),
    l(t('Asset Pie Chart'), 'ledger/reports/asset-pie'),
  );
  $output = array(
    '#theme' => 'item_list',
    '#items' => $items,
  );
  return $output;
}

